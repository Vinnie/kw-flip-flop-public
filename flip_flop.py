"""
Simple ResNet Trainers

CommandLine:
    python flip_flop.py --method=eightteen
    python flip_flop.py --method=fifty --lr 0.01
"""
import copy
import random
import json

import numpy as np
from lop.algos.res_gnt import ResGnT
import torchvision
import torch
import torch_liberator
import torch.nn as nn
import torchvision.transforms as transforms
from torchvision.models.feature_extraction import get_graph_node_names, create_feature_extractor
from torch.utils.data import Subset, DataLoader, random_split
import time
import scriptconfig as scfg
import kwutil
import ubelt as ub
import pickle
import rich
import rich.markup
# import torch.optim as optim
import torch_optimizer
import torchvision_modified_resnet
from torchvision_modified_resnet import kaiming_init_resnet_module


class FlipFLopCLI(scfg.DataConfig):
    method = scfg.Value(
        "eightteen",
        choices=["eightteen", "fifty", "thirtyfour",
                 "eightteen_small", "thirtyfour_small",
                 "half", "full"],
        help="Choose which method to implement, eightteen and fifty are default resnets",
    )
    method_note = scfg.Value(
        "",
        help="Additional note on the method, to use in the filepath. For example, distinguishing between different versions of 'half'."
    )
    flipflop_freq = scfg.Value(
        1,
        help="For full/half methods, number of epochs to train a model before flipflopping",
    )
    resnetA = scfg.Value(
        "eightteen",
        choices=["eightteen", "fifty", "thirtyfour", "eightteen_small", "thirtyfour_small",],
        help="if half or full is selected allowe user to pick two networks to use torch_liberator on"
    )
    resnetB = scfg.Value(
        "fifty",
        choices=["eightteen", "fifty", "thirtyfour", "eightteen_small", "thirtyfour_small",],
        help="if half or full is selected allowe user to pick two networks to use torch_liberator on"
    )

    # Knowledge distillation parameters (only used in "half" method going from resnetB to resnetA)
    distill_epochs = scfg.Value(
        0,
        help="Num epochs to distill (only when going from resnetB to resnetA)"
    )
    T = scfg.Value(
        2,
        help="Distillation temp (see T in https://pytorch.org/tutorials/beginner/knowledge_distillation_tutorial.html)"
    )
    lambda_distillation = scfg.Value(
        0.25,
        help="Weight on distillation loss."
    )

    # Optimization parameters
    optimizer = scfg.Value(
        "SGD",
        choices=["SGD", "Adam", "AdamW", "lamb"],
        help="specify optimizer to be used"
        )
    lr = scfg.Value(1e-2, help="set Learning Rate")
    weight_decay = scfg.Value(5e-4, help="Weight decay")
    momentum = scfg.Value(0.9, help="Momentum")
    b0 = scfg.Value(0.9, help="specify Beta for Adam/Lamb")
    b1 = scfg.Value(0.999, help="specify 2nd Beta for Adam/Lamb")
    eps = scfg.Value(1e-06, help="specify number for stability ")
    additional_transform = scfg.Value(False, help="applies additonal transforms to the training dataset. right now random crops and flips")
    # Hardware
    gpu = scfg.Value(0, help="specify a gpu")
    out_path = scfg.Value('./training_flipflop', help="specify an output path")

    batches = scfg.Value(1000, help="size of batches")
    epochs = scfg.Value(1000, help="number of epochs")
    save_model = scfg.Value(True, help="Save Model After every epoch")
    workers = scfg.Value(8, help='number of dataloader workers')

    seed = scfg.Value(345, help="seed")

    # Dataset/experiment settings
    dataset = scfg.Value("cifar10", choices=["cifar10", "cifar100"],
                         help="Dataset to train/test on")
    experiment_type = scfg.Value("data_incremental",
                                 choices=["class_continual", "data_incremental"])
    num_tasks = scfg.Value(2, help="Number of tasks. If experiment_type is class_continual, each task will add 'num_classes/num_tasks' classes. If experiment_type is data_incremental, each task will add 'num_examples/num_tasks' examples.")

    # network resetting parameters
    reset_head = scfg.Value(False, help='Whether to reset head after each task')
    reset_network = scfg.Value(False, help='Wherther to reset entire network after each task')

    # Continual backprop parameters
    use_cbp = scfg.Value(False, help='Whether to use continual backprop')
    replacement_rate = scfg.Value(0.0, help="How many units should be replaced each epoch")
    utility_function = scfg.Value("weight", choices=["weight", "contribution"])
    maturity_threshold = scfg.Value(0, help="How many epochs to protect reinitialized units until they can be reset again")

    # Shrink and perturb parameters
    noise_std = scfg.Value(0.0, help="Noise for shrink-and-perturb")

    def __post_init__(self):
        """
        Checks to make sure config is internally-consistent, and adds
        any derived variables
        """
        assert (not self.use_cbp) or (self.replacement_rate > 0.0), "Replacement rate should be greater than 0."
        assert (not self.use_cbp) or (self.maturity_threshold > 0), "Maturity threshold should be greater than 0."
        self.perturb_weights_indicator = self.noise_std > 0.0
        if self.reset_head and self.reset_network:
            print(Warning("Resetting the whole network supersedes resetting the head of the network. There's no need to set both to True."))


def train(model, device, train_loader, optimizer, scheduler, epoch, config, resgnt, teacher=None):
    model.train()
    pman = kwutil.ProgressManager()
    loss_fn = nn.CrossEntropyLoss()
    with pman:
        train_prog = pman.progiter(train_loader, desc=f'Train Epoch: {epoch:3d}')
        for batch_idx, (data, target) in enumerate(train_prog):
            data, target = data.to(device), target.to(device)
            optimizer.zero_grad()
            if config.use_cbp:
                # If using cbp, model returns a dict of (name, feature) pairs.
                # The last one is the output. The remaining ones are post-ReLU
                # features (in order)
                feature_list = list(model.forward(data).values())
                output = feature_list[-1]
                current_features = [feat.detach() for feat in feature_list[:-1]]  # Detach intermediate features - no gradient needed on them
            else:
                # Normal forward pass (no cbp)
                output = model.forward(data)

            # Compute supervised loss
            loss = loss_fn(output, target)

            # If doing distillation: get logits from teacher model and compare it with
            # the current (student) model
            # https://pytorch.org/tutorials/beginner/knowledge_distillation_tutorial.html
            if teacher is not None:
                with torch.no_grad():
                    if config.use_cbp:
                        teacher_logits = list(teacher(data).values())[-1]
                    else:
                        teacher_logits = teacher(data)

                # Soften the student logits by applying softmax first and log() second
                soft_targets = nn.functional.softmax(teacher_logits / config.T, dim=-1)
                soft_prob = nn.functional.log_softmax(output / config.T, dim=-1)

                # Calculate the soft targets loss. Scaled by T**2 as suggested by the authors of the paper "Distilling the knowledge in a neural network"
                soft_targets_loss = torch.sum(soft_targets * (soft_targets.log() - soft_prob)) / soft_prob.size()[0] * (config.T**2)
                loss += soft_targets_loss * config.lambda_distillation

            loss.backward()
            optimizer.step()

            # If using cbp, run generate-and-test (reinitialize least useful features)
            if config.use_cbp:
                resgnt.gen_and_test(current_features)

            # If using shrink-and-perturb, inject noise
            inject_noise(model, config)

            if batch_idx % 10 == 0:
                train_prog.ensure_newline()

                # Compute accuracy just as a sanity-check
                pred = output.argmax(dim=1)
                acc = (pred == target).float().mean() * 100
                print(
                    "Train Epoch: {:3d} [{}/{} ({:.0f}%)]\tLoss: {:.6f}\tAcc: {:.2f}".format(
                        epoch,
                        batch_idx * len(data),
                        len(train_loader.dataset),
                        100.0 * batch_idx / len(train_loader),
                        loss.item(),
                        acc
                    )
                )
        scheduler.step()


def test(model, device, test_loader, epoch, config, name="Test"):
    model.eval()
    test_loss = 0
    correct = 0

    rows = []

    # use a different reduction because we want the total loss over all test
    # items, so we can divide it by the total number of items.
    loss_fn = nn.CrossEntropyLoss(reduction='sum')

    pman = kwutil.ProgressManager()
    with torch.no_grad(), pman:
        for data, target in pman.progiter(test_loader, desc=f'Test Epoch: {epoch:3d}'):
            data, target = data.to(device), target.to(device)
            if config.use_cbp:
                # If using cbp, model returns a dict of (name, feature) pairs.
                # The last one is the output. The remaining ones are post-ReLU
                # features (in order)
                feature_list = list(model(data).values())
                output = feature_list[-1]
            else:
                # Normal forward pass (no cbp)
                output = model(data)

            test_loss += loss_fn(output, target)
            pred = output.argmax(
                dim=1, keepdim=True
            )  # get the index of the max log-probability
            batch_probs = output.cpu().numpy()
            pred_idxs = pred.cpu().numpy().ravel()
            true_idxs = target.cpu().numpy().ravel()

            for pred_idx, true_idx, probs in zip(pred_idxs, true_idxs, batch_probs):
                score = probs[pred_idx]
                rows.append({
                    'true': true_idx,
                    'pred': pred_idx,
                    'score': score,
                })
            #correct += pred.eq(target.view_as(pred)).sum().item()
            # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True)  # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()
    test_loss /= len(test_loader.dataset)

    print('\n{} set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        name, test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))

    import pandas as pd
    df = pd.DataFrame(rows)
    from kwcoco.metrics import confusion_vectors

    cfsn_vecs = confusion_vectors.ConfusionVectors.from_arrays(
        true=df['true'].values,
        pred=df['pred'].values,
        score=df['score'].values,
        classes=test_loader.dataset.classes,
    )
    report = cfsn_vecs.classification_report()
    confusion = report['confusion']
    metrics = report['metrics']
    PRINT_REPORT = (name == "Test")
    if PRINT_REPORT:
        print('Test Confusion')
        print('-------------')
        rich.print(confusion)
        print('Test Metrics')
        print('------------')
        rich.print(metrics)
    combined_metrics = metrics.loc['combined']
    return combined_metrics['f1']


def create_model(config, num_classes):
    if config == "fifty":
        return torchvision.models.resnet50(num_classes=num_classes)
    elif config == "eightteen":
        return torchvision.models.resnet18(num_classes=num_classes)
    elif config == "thirtyfour":
        return torchvision.models.resnet34(num_classes=num_classes)
    elif config == "eightteen_small":
        m = torchvision_modified_resnet.build_resnet18(num_classes=num_classes, norm_layer=torch.nn.BatchNorm2d)
        m.apply(kaiming_init_resnet_module)
        return m
    elif config == "thirtyfour_small":
        m = torchvision_modified_resnet.build_resnet34(num_classes=num_classes, norm_layer=torch.nn.BatchNorm2d)
        m.apply(kaiming_init_resnet_module)
        return m


def config_optimizer(model, config):
    # Set up optimizer
    if config.optimizer == "SGD":
        optimizer = torch.optim.SGD(model.parameters(), lr=config.lr, momentum=config.momentum, weight_decay=config.weight_decay)
    elif config.optimizer == "Adam":
        optimizer = torch.optim.Adam(model.parameters(), lr=config.lr, betas=(config.b0, config.b1), weight_decay=config.weight_decay)
    elif config.optimizer == "AdamW":
        optimizer = torch.optim.AdamW(model.parameters(), lr=config.lr, betas=(config.b0, config.b1), weight_decay=config.weight_decay)
    elif config.optimizer == "lamb":
        optimizer = torch_optimizer.lamb.Lamb(model.parameters(), lr=config.lr, betas=(config.b0, config.b1), eps=config.eps, weight_decay=config.weight_decay)
    else:
        raise ValueError("Invalid config.optimizer (choices: SGD, Adam, AdamW, lamb)")
    return optimizer


def inject_noise(net, config):
    """
    Adds a small amount of random noise to the parameters of the network

    Source: https://github.com/shibhansh/loss-of-plasticity/blob/main/lop/incremental_cifar/incremental_cifar_experiment.py
    """
    if not config.perturb_weights_indicator:
        return

    with torch.no_grad():
        for param in net.parameters():
            param.add_(torch.randn(param.size(), device=param.device) * config.noise_std)


def main():
    """
    IGNORE: from flip_flop import *
    """
    config = FlipFLopCLI.cli(cmdline=1, special_options=False)
    rich.print(rich.markup.escape(ub.urepr(config)))

    if 1:
        from geowatch.utils import process_context
        from kwcoco.util import util_json
        jsonified = util_json.ensure_json_serializable(config.to_dict())
        proc_context = process_context.ProcessContext(
            name='flip_flop.py',
            type='process',
            config=jsonified,
            track_emissions=True,
            # config['track_emissions'],
        )
        proc_context.start()
        proc_context.add_disk_info(config.out_path)
    else:
        proc_context = None

    method = config.method

    time0 = time.time()
    # Directly load resnet50 state into a model that has it as an embedded subnetwork
    # load partial state returns information about what it did
    # Training settings
    batch_size = config.batches
    epochs = config.epochs
    no_cuda = False
    use_cuda = not no_cuda and torch.cuda.is_available()

    # For reproducibility (TODO change to geowatch seed_everything?)
    # import pytorch_lightning as pl
    # pl.seed_everything(config.seed)

    torch.manual_seed(config.seed)
    torch.cuda.manual_seed(config.seed)
    np.random.seed(config.seed)
    random.seed(config.seed)

    device = torch.device(f"cuda:{config.gpu}" if use_cuda else "cpu")

    if proc_context is not None:
        proc_context.add_device_info(device)

    print(f'device={device}')
    res_val = []
    res1 = []
    res2 = []
    if config.additional_transform:
        # Training Data augmentation
        transform_train = transforms.Compose(
            [
                transforms.RandomCrop(32, padding=4),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
            ]
        )
        print("Augmentation")
    else:
        # Training Data augmentation
        transform_train = transforms.Compose(
            [
                transforms.ToTensor(),
                transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
            ]
        )

    # Testing Data preparation
    transform_test = transforms.Compose(
        [
            transforms.ToTensor(),
            transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
        ]
    )

    # Set up datasets
    if config.dataset == "cifar10":
        train_val_set = torchvision.datasets.CIFAR10(
            root="./data", train=True, download=True, transform=transform_train
        )
        testset = torchvision.datasets.CIFAR10(
            root="./data", train=False, download=True, transform=transform_test
        )

        # Overwrite class labels to use shorter terms for "airplane" and "automobile"
        CIFAR10_CLASSES = (
            "plane",
            "car",
            "bird",
            "cat",
            "deer",
            "dog",
            "frog",
            "horse",
            "ship",
            "truck",
        )
        classes = CIFAR10_CLASSES
        train_val_set.classes = CIFAR10_CLASSES
        testset.classes = CIFAR10_CLASSES

    elif config.dataset == "cifar100":
        train_val_set = torchvision.datasets.CIFAR100(
            root="./data", train=True, download=True, transform=transform_train
        )
        testset = torchvision.datasets.CIFAR100(
            root="./data", train=False, download=True, transform=transform_test
        )
        classes = train_val_set.classes
    else:
        raise ValueError("dataset not supported (choose cifar10 or cifar100)")

    # Split "training" set into train/validation splits.
    # Use a generator so it is exactly reproducible
    generator = torch.Generator().manual_seed(config.seed)
    trainset, valset = random_split(train_val_set, [2 / 3., 1 / 3.], generator=generator)

    # "random_split" does not preserve custom fields, such as
    # "targets" (label of each example) and "classes" (list of
    # class names). Manually port those over to trainset and valset.
    # https://github.com/pytorch/pytorch/issues/97944
    trainset.targets = torch.tensor([train_val_set.targets[i] for i in trainset.indices], device=device)
    valset.targets = torch.tensor([train_val_set.targets[i] for i in valset.indices], device=device)
    trainset.classes = train_val_set.classes
    valset.classes = train_val_set.classes

    # Do not augment the validation images.
    valset.transform = transform_test

    # Continual learning simulations: order in which to add classes or examples
    class_order = torch.randperm(len(train_val_set.classes), generator=generator)
    train_order = torch.randperm(len(trainset), generator=generator)
    val_order = torch.randperm(len(valset), generator=generator)
    epochs_per_task = config.epochs // config.num_tasks

    # Subset the train/validation datasets
    if config.experiment_type == "class_continual":
        # Add additional classes
        new_classes_per_task = len(trainset.classes) // config.num_tasks
        current_num_classes = new_classes_per_task
        curr_classes = class_order[:current_num_classes]

        # Select indices of examples that match those classes
        train_indices = torch.isin(trainset.targets, curr_classes).nonzero().flatten()
        trainset_task = Subset(trainset, train_indices)
        val_indices = torch.isin(valset.targets, curr_classes).nonzero().flatten()
        valset_task = Subset(valset, val_indices)
    elif config.experiment_type == "data_incremental":
        # Add additional datapoints
        current_fraction = (1 / config.num_tasks)
        n_train = int(current_fraction * len(train_order))
        n_val = int(current_fraction * len(val_order))
        trainset_task = Subset(trainset, train_order[:n_train])
        valset_task = Subset(valset, val_order[:n_val])
    else:
        raise ValueError("Invalid experiment_type")

    # Also add targets/classes to the subset. We can use simpler
    # indexing syntax as trainset.targets/valset.targets are already Tensors.
    trainset_task.targets = trainset.targets[trainset_task.indices]
    valset_task.targets = valset.targets[valset_task.indices]
    trainset_task.classes = train_val_set.classes
    valset_task.classes = train_val_set.classes

    # Create dataloaders
    train_loader = DataLoader(
        trainset_task, batch_size=batch_size, shuffle=True, num_workers=config.workers
    )
    val_loader = DataLoader(
        valset_task, batch_size=batch_size, shuffle=False, num_workers=config.workers
    )
    test_loader = DataLoader(
        testset, batch_size=batch_size, shuffle=False, num_workers=config.workers
    )
    train_loader.classes = classes
    val_loader.classes = classes
    test_loader.classes = classes
    num_classes = len(classes)

    # Set up models
    if config.method in ["eightteen", "thirtyfour", "fifty",
                         "eightteen_small", "thirtyfour_small", "fifty_small"]:
        if config.use_cbp:
            # Set up generate-and-test algorithm (continual backprop).
            # Computes utilty of each neuron and resets the least-useful ones
            model = create_model(config.method, num_classes=num_classes)

            # First, use torchvision's feature extractor to convert both models
            # into models that reutrn post-activation features.
            train_nodes, eval_nodes = get_graph_node_names(model)
            print("Train nodes", train_nodes)

            # Replace last post-activaiton with flattened. "fc" returns the output logits
            post_activations = [n for n in train_nodes if "relu" in n][:-1] + ["flatten", "fc"]
            print("Post activations", post_activations)
            return_nodes = {k: k for k in post_activations}
            model = create_feature_extractor(model, return_nodes=return_nodes)
            model_init = copy.deepcopy(model)
            model.to(device)
            model_init.to(device)

            # Set up ResGnT objects on top of the networks, which can track
            # the utility of each unit and has functions to replace low-utility units.
            resgnt = ResGnT(net=model,
                            hidden_activation="relu",
                            replacement_rate=config.replacement_rate,
                            decay_rate=0.99,
                            util_type=config.utility_function,
                            maturity_threshold=config.maturity_threshold,
                            device=device)
        else:
            # Create the single-model (no generate-and-test)
            model = create_model(config.method, num_classes=num_classes)
            model.to(device)
            resgnt = None

    # Initialize model18 from model50
    elif config.method == "half" or config.method == "full":
        if config.use_cbp:
            # Set up generate-and-test algorithm (continual backprop) for modelA.
            # See comment above
            modelA = create_model(config.resnetA, num_classes=num_classes)
            train_nodes_A, eval_nodes_A = get_graph_node_names(modelA)
            post_activations_A = [n for n in train_nodes_A if "relu" in n][:-1] + ["flatten", "fc"]
            return_nodes_A = {k: k for k in post_activations_A}
            modelA = create_feature_extractor(modelA, return_nodes=return_nodes_A)
            modelA.to(device)

            # Repeat for modelB
            modelB = create_model(config.resnetB, num_classes=num_classes)
            train_nodes_B, eval_nodes_B = get_graph_node_names(modelB)
            post_activations_B = [n for n in train_nodes_B if "relu" in n][:-1] + ["flatten", "fc"]
            return_nodes_B = {k: k for k in post_activations_B}
            modelB = create_feature_extractor(modelB, return_nodes=return_nodes_B)
            modelB.to(device)

            # Set up ResGnT objects on top of the networks, which can track
            # the utility of each unit and has functions to replace low-utility units.
            resgntA = ResGnT(net=modelA,
                             hidden_activation="relu",
                             replacement_rate=config.replacement_rate,
                             decay_rate=0.99,
                             util_type=config.utility_function,
                             maturity_threshold=config.maturity_threshold,
                             device=device)
            resgntB = ResGnT(net=modelB,
                             hidden_activation="relu",
                             replacement_rate=config.replacement_rate,
                             decay_rate=0.99,
                             util_type=config.utility_function,
                             maturity_threshold=config.maturity_threshold,
                             device=device)
        else:
            # Create both models (no generate-and-test)
            modelA = create_model(config.resnetA, num_classes=num_classes)
            modelA.to(device)
            modelB = create_model(config.resnetB, num_classes=num_classes)
            modelB.to(device)
            resgntA, resgntB = None, None

        torch_liberator.load_partial_state(
            modelA, modelB.state_dict(), association="isomorphism", verbose=0
        )

    # Keep track of the best model
    best_val_f1 = 0.
    best_epoch = -1

    # setup learning rate scheduler
    if config.method in ["eightteen", "fifty", "thirtyfour",
                         "eightteen_small", "thirtyfour_small", "fifty_small"]:
        optimizer = config_optimizer(model, config)
        # scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=[150, 225, 270], gamma=0.1)
        scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=200)
    else:
        optimizerA = config_optimizer(modelA, config)
        optimizerB = config_optimizer(modelB, config)
        schedulerA = torch.optim.lr_scheduler.MultiStepLR(optimizerA, milestones=[150, 225, 270], gamma=0.1)
        schedulerB = torch.optim.lr_scheduler.MultiStepLR(optimizerB, milestones=[150, 225, 270], gamma=0.1)

    # Ensure all output directories exist
    out_dpath = ub.Path(config.out_path).ensuredir()
    if config.method == "full" or config.method == "half":
        ub.Path(out_dpath + rf"/{config.resnetA}").ensuredir()
        ub.Path(out_dpath + rf"/{config.resnetB}").ensuredir()
    else:
        ub.Path(out_dpath + rf"/{config.method}").ensuredir()

    # Dump out_path
    with open(out_dpath / "train_config.json", "w") as config_file:
        config.dump(config_file)

    (out_dpath / 'process_context_start.json').write_text(json.dumps(proc_context.obj))

    for epoch in range(1, epochs + 1):
        print(f"Epoch {epoch}, Train set {len(train_loader.dataset)}, Val set {len(val_loader.dataset)}")
        if config.method == "full":
            if ((epoch - 1) // config.flipflop_freq) % 2 == 1:
                # Load model50 into model18, then train model18
                torch_liberator.load_partial_state(
                    modelA, modelB.state_dict(), association="isomorphism", verbose=0
                )
                train(modelA, device, train_loader, optimizerA, schedulerA, epoch, config, resgntA)
                res_val.append(test(modelA, device, val_loader, epoch, config, name="Validation"))
            else:
                # Load model18 into model50, then train model50
                torch_liberator.load_partial_state(
                    modelB, modelA.state_dict(), association="isomorphism", verbose=0
                )
                train(modelB, device, train_loader, optimizerB, schedulerB, epoch, config, resgntB)
                res_val.append(test(modelB, device, val_loader, epoch, config, name="Validation"))

            # Test and save both models
            res1.append(test(modelA, device, test_loader, epoch, config))
            res2.append(test(modelB, device, test_loader, epoch, config))
            torch.save(
                modelA.state_dict(), out_dpath / rf"{config.resnetA}/{config.resnetA}_{epoch}.pt"
            )
            torch.save(
                modelB.state_dict(), out_dpath / rf"{config.resnetB}/{config.resnetB}_{epoch}.pt"
            )

        elif config.method == "half":
            if ((epoch - 1) // config.flipflop_freq) % 2 == 1:
                if ((epoch - 1) % config.flipflop_freq) < config.distill_epochs:
                    teacher = modelB
                else:
                    teacher = None

                # Reset model18
                modelA_init = create_model(config.resnetA, num_classes=num_classes).to(device)
                modelA.load_state_dict(modelA_init.state_dict())

                # Load model50 into model18, then train model18
                torch_liberator.load_partial_state(
                    modelA, modelB.state_dict(), association="isomorphism", verbose=0
                )
                train(modelA, device, train_loader, optimizerA, schedulerA, epoch, config, resgntA, teacher=teacher)
                res_val.append(test(modelA, device, val_loader, epoch, config, name="Validation"))

            else:
                # # Reset model50
                modelB_init = create_model(config.resnetB, num_classes=num_classes).to(device)
                modelB.load_state_dict(modelB_init.state_dict())

                # Load model18 into model50, then train model50
                torch_liberator.load_partial_state(
                    modelB, modelA.state_dict(), association="isomorphism", verbose=0
                )
                train(modelB, device, train_loader, optimizerB, schedulerB, epoch, config, resgntB)
                res_val.append(test(modelB, device, val_loader, epoch, config, name="Validation"))

            # Test and save both models
            res1.append(test(modelA, device, test_loader, epoch, config))
            res2.append(test(modelB, device, test_loader, epoch, config))

            torch.save(
                modelA.state_dict(), out_dpath / rf"{config.resnetA}/{config.resnetA}_{epoch}.pt"
            )
            torch.save(
                modelB.state_dict(), out_dpath / rf"{config.resnetB}/{config.resnetB}_{epoch}.pt"
            )

        elif config.method in ["eightteen", "thirtyfour", "fifty",
                               "eightteen_small", "thirtyfour_small", "fifty_small"]:
            # Standard training (no flip-flop)
            train(model, device, train_loader, optimizer, scheduler, epoch, config, resgnt)
            res_val.append(test(model, device, val_loader, epoch, config, name="Validation"))
            res1.append(test(model, device, test_loader, epoch, config))
            torch.save(
                model.state_dict(), out_dpath / rf"{config.method}/{config.method}_{epoch}.pt"
            )

        else:
            raise ValueError("config.method not supported (choices: eightteen, fifty, half, full)")

        # Every "epochs_per_task" epochs, add new data. TODO refactor
        # into a method since it's currently done twice
        if epoch % epochs_per_task == 0:
            if config.experiment_type == "class_continual":
                # Add additional classes
                current_num_classes += new_classes_per_task
                curr_classes = class_order[:current_num_classes]

                # Select indices of examples that match those classes
                train_indices = torch.isin(trainset.targets, curr_classes).nonzero().flatten()
                trainset_task = Subset(trainset, train_indices)
                val_indices = torch.isin(valset.targets, curr_classes).nonzero().flatten()
                valset_task = Subset(valset, val_indices)
            elif config.experiment_type == "data_incremental":
                # Add additional datapoints
                current_fraction += (1 / config.num_tasks)
                n_train = int(current_fraction * len(train_order))
                n_val = int(current_fraction * len(val_order))
                trainset_task = Subset(trainset, train_order[:n_train])
                valset_task = Subset(valset, val_order[:n_val])
            else:
                raise ValueError("Invalid experiment_type")

            # Also add targets/classes to the subset
            trainset_task.targets = trainset.targets[trainset_task.indices]
            valset_task.targets = valset.targets[valset_task.indices]
            trainset_task.classes = train_val_set.classes
            valset_task.classes = train_val_set.classes

            # Reset dataloaders to load the new data subset
            train_loader = DataLoader(
                trainset_task, batch_size=batch_size, shuffle=True, num_workers=config.workers
            )
            val_loader = DataLoader(
                valset_task, batch_size=batch_size, shuffle=False, num_workers=config.workers
            )

            # Reset networks. Only supported if they are small.
            model_list = []  # List of "small" models to reset
            if config.method in ["half", "full"]:
                if "small" in config.resnetA:
                    model_list.append(modelA)
                if "small" in config.resnetB:
                    model_list.append(modelB)
            else:
                if "small" in config.method:
                    model_list = [model]

            if config.reset_head:
                assert len(model_list) >= 1, "reset_head is True but resetting is only supported for small models."
                for model_to_reset in model_list:
                    kaiming_init_resnet_module(model_to_reset.fc)
            if config.reset_network:
                assert len(model_list) >= 1, "reset_head is True but resetting is only supported for small models."
                for model_to_reset in model_list:
                    model_to_reset.apply(kaiming_init_resnet_module)

        # If it's the last task, save the best performing model
        if epoch >= config.epochs - epochs_per_task:
            if res_val[-1] > best_val_f1:
                best_epoch = epoch
                best_val_f1 = res_val[-1]
                if len(res2) > 0:
                    best_test_f1 = max(res1[-1], res2[-1])
                else:
                    best_test_f1 = res1[-1]

                if config.method in ["eightteen", "thirtyfour", "fifty",
                                     "eightteen_small", "thirtyfour_small", "fifty_small"]:
                    torch.save(
                        model.state_dict(), out_dpath / rf"{config.method}/{config.method}_BEST.pt"
                    )
                else:
                    torch.save(
                        modelA.state_dict(), out_dpath / rf"{config.resnetA}/{config.resnetA}_BEST.pt"
                    )
                    torch.save(
                        modelB.state_dict(), out_dpath / rf"{config.resnetB}/{config.resnetB}_BEST.pt"
                    )

    # Dump results
    with open(rf'{method}_val.PICKLE', 'wb') as F:
        pickle.dump(res_val, F)

    if config.method in ["eightteen", "thirtyfour", "fifty",
                         "eightteen_small", "thirtyfour_small", "fifty_small"]:
        with open(rf'{method}.PICKLE', 'wb') as F:
            pickle.dump(res1, F)

    if config.method == 'half':
        with open(rf'{method}_{config.resnetA}.PICKLE', 'wb') as F:
            pickle.dump(res1, F)
        with open(rf'{method}_{config.resnetB}.PICKLE', 'wb') as F:
            pickle.dump(res2, F)

    if config.method == 'full':
        with open(rf'{method}_{config.resnetA}.PICKLE', 'wb') as F:
            pickle.dump(res1, F)
        with open(rf'{method}_{config.resnetB}.PICKLE', 'wb') as F:
            pickle.dump(res2, F)

    proc_context.stop()
    (out_dpath / 'process_context_end.json').write_text(json.dumps(process_context.obj))

    time1 = time.time()
    print("Training and Testing total excution time is: %s seconds " % (time1 - time0))
    print(f"Best epoch {best_epoch}: Val F1 {best_val_f1}, Test F1 {best_test_f1}")


if __name__ == "__main__":
    main()
