import os
import scriptconfig as scfg
import ubelt as ub
import cmd_queue


polygen_template = ub.codeblock(
        r"""
        python flip_flop.py \
            --seed '{seed}' \
            --method '{method}' \
            --resnetA eightteen_small \
            --resnetB thirtyfour_small \
            --gpu {gpu} \
            --out_path {path} \
            --num_tasks 5 \
            --epochs 500 \
            --batches 128 \
            --additional_transform True \
            --optimizer Adam \
            --lr 1e-3 \
            --weight_decay 0 {extra}
        """
)

for SEED in [0]:
    ROOT_DIR = ub.Path(f"/data2/resnet_results_joshua/test_sota_seed{SEED}")
    # # ROOT_DIR = ub.Path("./results")
    # (ROOT_DIR / "thirtyfour/resnet34").ensuredir()
    # (ROOT_DIR / "eightteen/resnet18").ensuredir()
    # (ROOT_DIR / "half/resnet34").ensuredir()
    # (ROOT_DIR / "half/resnet18").ensuredir()
    # (ROOT_DIR / "full/resnet34").ensuredir()
    # (ROOT_DIR / "full/resnet18").ensuredir()
    out_path = [ROOT_DIR / 'eightteen_small/', ROOT_DIR / 'thirtyfour_small/',
                ROOT_DIR / 'half5/', ROOT_DIR / 'half5_cbp/']
    extra = ['', '', '', '--flipflop_freq 5', '--flipflop_freq 5 --use_cbp True --replacement_rate 0.00001 --utility_function contribution --maturity_threshold 1000']
    method = ['eightteen_small', 'thirtyfour_small', 'half', 'half']

    queue = cmd_queue.Queue.create(
            backend="tmux",
            #backend="serial",
            size=4,
        )
    for i in range(len(out_path)):
        fmtkw = {
            'seed': SEED,
            'method': method[i],
            'gpu': i,
            'path': out_path[i],
            'extra': extra[i]
        }
        cmd = polygen_template.format(**fmtkw)
        queue.submit(cmd, name=f'resnet_{os.path.basename(out_path[i])}')
    queue.print_graph()
    queue.print_commands()
    queue.run()
