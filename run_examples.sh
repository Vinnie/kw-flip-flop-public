#!/bin/bash
__doc__="
Make sure to change out_path, seed, gpu
"
if [[ "$USER" == jon*crall ]]; then
    RESULT_DPATH="$HOME/flash/flipflop"
    mkdir -p "$RESULT_DPATH"
    cd "$RESULT_DPATH"
else
    RESULT_DPATH="/data2/resnet_results_joshua"
fi
REPO_DPATH=$HOME/code/kw_flip_flop
SEED=0
GPU=0

cd "$REPO_DPATH"

cmd_queue new "flip_flip_queue"

# 1) Single model (small ResNet18), on entire dataset.
# Similar to SOTA
cmd_queue submit "flip_flip_queue" -- \
python flip_flop.py \
    --method eightteen_small \
    --num_tasks 1 \
    --epochs 500 \
    --batches 128 \
    --additional_transform True \
    --optimizer SGD \
    --lr 1e-2 \
    --out_path "$RESULT_DPATH/test_sota" \
    --seed $SEED \
    --gpu $GPU

# 2) Single model (small ResNet18), continual (10 tasks).
# Completely resetting after every task. The last task
# should be the same as (1).
cmd_queue submit "flip_flip_queue" -- \
python flip_flop.py \
    --method eightteen_small \
    --reset_network True \
    --num_tasks 10 \
    --epochs 500 \
    --batches 128 \
    --additional_transform True \
    --optimizer SGD \
    --lr 1e-2 \
    --out_path "$RESULT_DPATH/test_continual_reset" \
    --seed $SEED \
    --gpu $GPU

# 3) Single model (small ResNet18), continual (10 tasks)
cmd_queue submit "flip_flip_queue" -- \
python flip_flop.py \
    --method eightteen_small \
    --num_tasks 10 \
    --epochs 500 \
    --batches 128 \
    --additional_transform True \
    --optimizer SGD \
    --lr 1e-2 \
    --out_path "$RESULT_DPATH/test_continual" \
    --seed $SEED \
    --gpu $GPU

# 4) Half flipflop every epoch (small ResNet18 to small ResNet34), continual (10 tasks)
cmd_queue submit "flip_flip_queue" -- \
python flip_flop.py \
    --method half \
    --resnetA eightteen_small \
    --resnetB thirtyfour_small \
    --flipflop_freq 1 \
    --num_tasks 10 \
    --epochs 500 \
    --batches 128 \
    --additional_transform True \
    --optimizer SGD \
    --lr 1e-2 \
    --out_path "$RESULT_DPATH/test_continual_half" \
    --seed $SEED \
    --gpu $GPU

# 5) Half flipflop every 5 epochs WITH distillation, continual (10 tasks)
cmd_queue submit "flip_flip_queue" -- \
python flip_flop.py \
    --method half \
    --resnetA eightteen_small \
    --resnetB thirtyfour_small \
    --flipflop_freq 5 \
    --distill_epochs 3 \
    --num_tasks 10 \
    --epochs 500 \
    --batches 128 \
    --additional_transform True \
    --optimizer SGD \
    --lr 1e-2 \
    --out_path "$RESULT_DPATH/test_continual_half5" \
    --seed $SEED \
    --gpu $GPU

# 6) Half flipflop every epoch + continual backprop, continual (10 tasks)
cmd_queue submit "flip_flip_queue" -- \
python flip_flop.py \
    --method half \
    --resnetA eightteen_small \
    --resnetB thirtyfour_small \
    --use_cbp True \
    --replacement_rate 0.00001 \
    --utility_function contribution \
    --maturity_threshold 1000 \
    --num_tasks 10 \
    --epochs 500 \
    --batches 128 \
    --additional_transform True \
    --optimizer SGD \
    --lr 1e-2 \
    --out_path "$RESULT_DPATH/test_continual_half_cbp" \
    --seed $SEED \
    --gpu $GPU

# 7) Half flipflop every epoch + shrink-and-perturb, continual (10 tasks)
cmd_queue submit "flip_flip_queue" -- \
python flip_flop.py \
    --method half \
    --resnetA eightteen_small \
    --resnetB thirtyfour_small \
    --noise_std 0.00001 \
    --num_tasks 10 \
    --epochs 500 \
    --batches 128 \
    --additional_transform True \
    --optimizer SGD \
    --lr 1e-2 \
    --out_path "$RESULT_DPATH/test_continual_half_sp" \
    --seed $SEED \
    --gpu $GPU


cmd_queue show "flip_flip_queue"
# Execute your queue.
#cmd_queue run "flip_flip_queue" --backend=serial
cmd_queue run "flip_flip_queue" --backend=tmux --workers=4 --gpus="0,1"


# Final plotting
# For now, in gen_matrix.py, you need to manually assign SUBDIRS, METHODS, MODELS to align with the runs above.
# TODO try to automate more of this
# python3 gen_matrix.py --resnetA eightteen_small --resnetB thirtyfour_small --epochs 500 --plot_freq 100 --result_dir $RESULT_DPATH
