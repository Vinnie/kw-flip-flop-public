"""
Simple ResNet Trainers

CommandLine:
    python flip_flop.py --method=eightteen
    python flip_flop.py --method=fifty --lr 0.01
"""

import random
import copy
import numpy as np
from lop.algos.res_gnt import ResGnT

# from lop.algos.res_gnt import ResGnT
import torchvision
import torch
import torch_liberator
import torch.nn as nn
import torchvision.transforms as transforms
from torch.utils.data import Subset, DataLoader, random_split
from torchvision.models.feature_extraction import (
    get_graph_node_names,
    create_feature_extractor,
)
import time
import scriptconfig as scfg
import kwutil
import ubelt as ub
import pickle
import rich
import rich.markup
import torch.optim as optim
import torch_optimizer
import lightning as L
import torchmetrics
from lightning.pytorch.callbacks import Callback, ModelCheckpoint
import resnet
import torchvision_modified_resnet

class JustDoTestEveryEpoch(Callback):
    def __init__ (self, testset):
        self.testset=testset
    def on_train_epoch_end(self,trainer,pl_module):
        ...


def create_model(config, num_classes):
    if config == "fifty":
        return torchvision.models.resnet50(num_classes=num_classes)
    elif config == "eightteen":
        return torchvision.models.resnet18(num_classes=num_classes)
    elif config == "thirtyfour":
        return torchvision.models.resnet34(num_classes=num_classes)
    elif config == "eightteen_small":
        return torchvision_modified_resnet.build_resnet18(
            num_classes=num_classes, norm_layer=torch.nn.BatchNorm2d
        )
    elif config == "thirtyfour_small":
        return torchvision_modified_resnet.build_resnet34(
            num_classes=num_classes, norm_layer=torch.nn.BatchNorm2d
        )


def inject_noise(net, config):
    """
    Adds a small amount of random noise to the parameters of the network

    Source: https://github.com/shibhansh/loss-of-plasticity/blob/main/lop/incremental_cifar/incremental_cifar_experiment.py
    """
    if not config.perturb_weights_indicator:
        return

    with torch.no_grad():
        for param in net.parameters():
            param.add_(
                torch.randn(param.size(), device=param.device) * config.noise_std
            )


class LitResnetFullHalf(L.LightningModule):

    def __init__(self, resnetA, resnetB, config, classes, resgntA, resgntB):
        super().__init__()
        self.resnetA = resnetA
        self.resnetB = resnetB
        self.config = config
        self.classes = classes
        self.teacher = None
        self.epoch = 0
        self.rowsA = []
        self.rowsB = []
        info = torch_liberator.load_partial_state(
            self.resnetA,
            self.resnetB.state_dict(),
            association="isomorphism",
            verbose=0,
        )
        self.count = 0
        if self.config.optimizer == "Adam":
            self.intializeA = optim.Adam(
                self.resnetA.parameters(),
                lr=self.config.lr,
                betas=(self.config.b0, self.config.b1),
                weight_decay=self.config.weight_decay,
            )
        elif self.config.optimizer == "SGD":
            self.intializeA = optim.SGD(
                self.resnetA.parameters(),
                lr=self.config.lr,
                momentum=self.config.momentum,
                weight_decay=self.config.weight_decay,
            )
        elif self.config.optimizer == "AdamW":
            self.intializeA = torch.optim.AdamW(
                self.resnetA.parameters(),
                lr=self.config.lr,
                betas=(self.config.b0, self.config.b1),
                weight_decay=self.config.weight_decay,
            )
        elif self.config.optimizer == "lamb":
            self.intializeA = torch_optimizer.lamb.Lamb(
                self.resnetA.parameters(),
                lr=self.config.lr,
                betas=(self.config.b0, self.config.b1),
                eps=self.config.eps,
                weight_decay=self.config.weight_decay,
            )

        if self.config.optimizer == "Adam":
            self.intializeB = optim.Adam(
                self.resnetA.parameters(),
                lr=self.config.lr,
                betas=(self.config.b0, self.config.b1),
                weight_decay=self.config.weight_decay,
            )
        elif self.config.optimizer == "SGD":
            self.intializeB = optim.SGD(
                self.resnetA.parameters(),
                lr=self.config.lr,
                momentum=self.config.momentum,
                weight_decay=self.config.weight_decay,
            )
        elif self.config.optimizer == "AdamW":
            self.intializeB = torch.optim.AdamW(
                self.resnetA.parameters(),
                lr=self.config.lr,
                betas=(self.config.b0, self.config.b1),
                weight_decay=self.config.weight_decay,
            )
        elif self.config.optimizer == "lamb":
            self.intializeB = torch_optimizer.lamb.Lamb(
                self.resnetA.parameters(),
                lr=self.config.lr,
                betas=(self.config.b0, self.config.b1),
                eps=self.config.eps,
                weight_decay=self.config.weight_decay,
            )

    def test_step(self, batch, batch_idx):
        if self.count == 0:
            loss_fn = nn.CrossEntropyLoss()
            x, y = batch
            output = self.resnetA.forward(x)
            loss = loss_fn(output, y, prog_bar=True)
            # Logging to TensorBoard (if installed) by default
            self.log("test_loss1", loss)
        elif self.count == 1:
            loss_fn = nn.CrossEntropyLoss()
            x, y = batch
            output = self.resnetB.forward(x)
            loss = loss_fn(output, y)
            # Logging to TensorBoard (if installed) by default
            self.log("test_loss2", loss, prog_bar=True)
        return loss

    def training_step(self, batch, batch_idx):
        # training_step defines the train loop.
        # it is independent of forward

        if self.count == 0:
            loss_fn = nn.CrossEntropyLoss()
            x, y = batch
            if self.config.use_cbp:
                # If using cbp, model returns a dict of (name, feature) pairs.
                # The last one is the output. The remaining ones are post-ReLU
                # features (in order)
                feature_list = list(self.resnetA.forward(x).values())
                output = feature_list[-1]
                current_features = [
                    feat.detach() for feat in feature_list[:-1]
                ]  # Detach intermediate features - no gradient needed on them
            else:
                # Normal forward pass (no cbp)
                output = self.resnetA.forward(x)

            loss = loss_fn(output, y)
            # Logging to TensorBoard (if installed) by default
            # If doing distillation: get logits from teacher model and compare it with
            # the current (student) model
            # https://pytorch.org/tutorials/beginner/knowledge_distillation_tutorial.html
            if self.teacher is not None:
                with torch.no_grad():
                    if self.config.use_cbp:
                        teacher_logits = list(self.teacher(x).values())[-1]
                    else:
                        teacher_logits = self.teacher(x)

                # Soften the student logits by applying softmax first and log() second
                soft_targets = nn.functional.softmax(
                    teacher_logits / self.config.T, dim=-1
                )
                soft_prob = nn.functional.log_softmax(output / self.config.T, dim=-1)

                # Calculate the soft targets loss. Scaled by T**2 as suggested by the authors of the paper "Distilling the knowledge in a neural network"
                soft_targets_loss = (
                    torch.sum(soft_targets * (soft_targets.log() - soft_prob))
                    / soft_prob.size()[0]
                    * (self.config.T**2)
                )
                loss += soft_targets_loss * self.config.lambda_distillation

            self.log("train_loss1", loss, prog_bar=True)
        elif self.count == 1:
            loss_fn = nn.CrossEntropyLoss()
            x, y = batch
            if self.config.use_cbp:
                # If using cbp, model returns a dict of (name, feature) pairs.
                # The last one is the output. The remaining ones are post-ReLU
                # features (in order)
                feature_list = list(self.resnetB.forward(x).values())
                output = feature_list[-1]
                current_features = [
                    feat.detach() for feat in feature_list[:-1]
                ]  # Detach intermediate features - no gradient needed on them
            else:
                # Normal forward pass (no cbp)
                output = self.resnetB.forward(x)

            loss = loss_fn(output, y)
            # Logging to TensorBoard (if installed) by default
            # If doing distillation: get logits from teacher model and compare it with
            # the current (student) model
            # https://pytorch.org/tutorials/beginner/knowledge_distillation_tutorial.html
            if self.teacher is not None:
                with torch.no_grad():
                    if self.config.use_cbp:
                        teacher_logits = list(self.teacher(x).values())[-1]
                    else:
                        teacher_logits = self.teacher(x)

                # Soften the student logits by applying softmax first and log() second
                soft_targets = nn.functional.softmax(
                    teacher_logits / self.config.T, dim=-1
                )
                soft_prob = nn.functional.log_softmax(output / self.config.T, dim=-1)

                # Calculate the soft targets loss. Scaled by T**2 as suggested by the authors of the paper "Distilling the knowledge in a neural network"
                soft_targets_loss = (
                    torch.sum(soft_targets * (soft_targets.log() - soft_prob))
                    / soft_prob.size()[0]
                    * (self.config.T**2)
                )
                loss += soft_targets_loss * self.config.lambda_distillation
            self.log("train_loss2", loss, prog_bar=True)

        return loss

    def on_train_epoch_end(self):
        self.epoch += 1
        self.teacher = None
        if self.config.method == "full":
            if ((self.epoch - 1) // self.config.flipflop_freq) % 2 == 1:
                # Load model50 into model18, then train model18
                torch_liberator.load_partial_state(
                    self.resnetA,
                    self.resnetB.state_dict(),
                    association="isomorphism",
                    verbose=0,
                )
                self.count = 0
            else:
                # Load model18 into model50, then train model50
                torch_liberator.load_partial_state(
                    self.resnetB,
                    self.resnetA.state_dict(),
                    association="isomorphism",
                    verbose=0,
                )
                self.count = 1
        elif self.config.method == "half":
            if ((self.epoch - 1) // self.config.flipflop_freq) % 2 == 1:

                if (
                    (self.epoch - 1) % self.config.flipflop_freq
                ) < self.config.distill_epochs:
                    self.teacher = self.resnetB

                # Reset model18
                modelA_init = create_model(
                    self.config.resnetA, num_classes=len(self.classes)
                )
                self.resnetA.load_state_dict(modelA_init.state_dict())

                # Load model50 into model18, then train model18
                torch_liberator.load_partial_state(
                    self.resnetA,
                    self.resnetB.state_dict(),
                    association="isomorphism",
                    verbose=0,
                )
                self.count = 0

            else:
                # # Reset model50
                modelB_init = create_model(
                    self.config.resnetB, num_classes=len(self.classes)
                )
                self.resnetB.load_state_dict(modelB_init.state_dict())

                # Load model18 into model50, then train model50
                torch_liberator.load_partial_state(
                    self.resnetB,
                    self.resnetA.state_dict(),
                    association="isomorphism",
                    verbose=0,
                )
                self.count = 1

    def validation_step(self, batch, batch_idx):
        if batch_idx == 0:
            self.rowsA = []
            self.rowsB = []

        # loss_fn = nn.CrossEntropyLoss()
        x, y = batch
        output = self.resnetA.forward(x)
        pred = output.argmax(dim=1, keepdim=True)
        # loss = loss_fn(output, y)
        batch_probs = output.cpu().numpy()
        pred_idxs = pred.cpu().numpy().ravel()
        true_idxs = y.cpu().numpy().ravel()

        # Logging to TensorBoard (if installed) by default
        for pred_idx, true_idx, probs in zip(pred_idxs, true_idxs, batch_probs):
            score = probs[pred_idx]
            self.rowsA.append(
                {
                    "true": true_idx,
                    "pred": pred_idx,
                    "score": score,
                }
            )
        # correct += pred.eq(target.view_as(pred)).sum().item()
        # sum up batch loss
        # loss_fn = nn.CrossEntropyLoss()
        output = self.resnetB.forward(x)
        pred = output.argmax(dim=1, keepdim=True)
        # loss = loss_fn(output, y)
        batch_probs = output.cpu().numpy()
        pred_idxs = pred.cpu().numpy().ravel()
        true_idxs = y.cpu().numpy().ravel()
        # Logging to TensorBoard (if installed) by default
        for pred_idx, true_idx, probs in zip(pred_idxs, true_idxs, batch_probs):
            score = probs[pred_idx]
            self.rowsB.append(
                {
                    "true": true_idx,
                    "pred": pred_idx,
                    "score": score,
                }
            )

        if batch_idx == len(batch):
            print(len(batch))
            import pandas as pd

            df = pd.DataFrame(self.rowsA)
            df2 = pd.DataFrame(self.rowsB)

            from kwcoco.metrics import confusion_vectors

            cfsn_vecs = confusion_vectors.ConfusionVectors.from_arrays(
                true=df["true"].values,
                pred=df["pred"].values,
                score=df["score"].values,
                classes=self.classes,
            )
            report = cfsn_vecs.classification_report()
            confusion = report["confusion"]
            metrics = report["metrics"]
            print("Test Confusion")
            print("-------------")
            # rich.print(confusion)
            print("Test Metrics")
            print("------------")
            # rich.print(metrics)

            cfsn_vecs = confusion_vectors.ConfusionVectors.from_arrays(
                true=df2["true"].values,
                pred=df2["pred"].values,
                score=df2["score"].values,
                classes=self.classes,
            )
            report = cfsn_vecs.classification_report()
            confusion = report["confusion"]
            metrics = report["metrics"]
            print("Test Confusion")
            print("-------------")
            # rich.print(confusion)
            print("Test Metrics")
            print("------------")
            # rich.print(metrics)
            combined_metrics = metrics.loc["combined"]
            # self.log("val_loss", loss,prog_bar=True)
            # return loss

    def configure_optimizers(self):
        if self.count == 0:
            optimizer = self.intializeA
            scheduler = optim.lr_scheduler.MultiStepLR(
                optimizer, milestones=[150, 225, 270], gamma=0.1
            )

        if self.count == 1:
            optimizer = self.intializeB
            scheduler = optim.lr_scheduler.MultiStepLR(
                optimizer, milestones=[150, 225, 270], gamma=0.1
            )

        return [optimizer], [scheduler]


class LitResnet(L.LightningModule):
    def __init__(self, resnetA, classes, config, resgnt):
        super().__init__()
        self.resnetA = resnetA
        self.resgnt = resgnt
        self.rows = []
        self.accuracy = torchmetrics.classification.Accuracy(
            task="multiclass", num_classes=len(classes)
        )
        self.classes = classes
        self.config = config
        print(self.config.optimizer)
        # self.save_hyperparameters()

    def training_step(self, batch, batch_idx):
        # training_step defines the train loop.
        # it is independent of forward
        loss = nn.CrossEntropyLoss()
        x, y = batch
        if self.config.use_cbp:
            # If using cbp, model returns a dict of (name, feature) pairs.
            # The last one is the output. The remaining ones are post-ReLU
            # features (in order)
            feature_list = list(self.resnetA.forward(x).values())
            output = feature_list[-1]
            current_features = [
                feat.detach() for feat in feature_list[:-1]
            ]  # Detach intermediate features - no gradient needed on them
        else:
            # Normal forward pass (no cbp)
            output = self.resnetA.forward(x)
        loss = loss(output, y)

        # Logging to TensorBoard (if installed) by default
        self.accuracy(output, y)
        if self.config.use_cbp:
            self.resgnt.gen_and_test(current_features)
        inject_noise(self.resnetA, self.config)

        # self.log("train_loss", loss,prog_bar=True)
        # self.log("train_acc_step",self.accuracy,prog_bar=True)

        return loss

    def on_train_epoch_end(self):
        ...
        # self.log("train_acc_epoch",self.accuracy,prog_bar=True)

    def test_step(self, batch, batch_idx):
        if batch_idx == 0:
            self.rows = []
        # loss_fn = nn.CrossEntropyLoss()
        x, y = batch
        output = self.resnetA.forward(x)
        pred = output.argmax(dim=1, keepdim=True)
        # loss = loss_fn(output, y)
        batch_probs = output.cpu().numpy()
        pred_idxs = pred.cpu().numpy().ravel()
        true_idxs = y.cpu().numpy().ravel()

        # Logging to TensorBoard (if installed) by default
        for pred_idx, true_idx, probs in zip(pred_idxs, true_idxs, batch_probs):
            score = probs[pred_idx]
            self.rows.append(
                {
                    "true": true_idx,
                    "pred": pred_idx,
                    "score": score,
                }
            )
        # correct += pred.eq(target.view_as(pred)).sum().item()
        # sum up batch loss
        if batch_idx == 9:
            import pandas as pd

            df = pd.DataFrame(self.rows)
            from kwcoco.metrics import confusion_vectors

            cfsn_vecs = confusion_vectors.ConfusionVectors.from_arrays(
                true=df["true"].values,
                pred=df["pred"].values,
                score=df["score"].values,
                classes=self.classes,
            )
            report = cfsn_vecs.classification_report()
            confusion = report["confusion"]
            metrics = report["metrics"]
            print("Test Confusion")
            print("-------------")
            rich.print(confusion)
            print("Test Metrics")
            print("------------")
            rich.print(metrics)
            combined_metrics = metrics.loc["combined"]
            # self.log("val_loss", loss,prog_bar=True)
            # return loss

    def validation_step(self, batch, batch_idx):
        if batch_idx == 0:
            self.rows = []
        # loss_fn = nn.CrossEntropyLoss()
        x, y = batch
        output = self.resnetA.forward(x)
        pred = output.argmax(dim=1, keepdim=True)
        # loss = loss_fn(output, y)
        batch_probs = output.cpu().numpy()
        pred_idxs = pred.cpu().numpy().ravel()
        true_idxs = y.cpu().numpy().ravel()

        # Logging to TensorBoard (if installed) by default
        for pred_idx, true_idx, probs in zip(pred_idxs, true_idxs, batch_probs):
            score = probs[pred_idx]
            self.rows.append(
                {
                    "true": true_idx,
                    "pred": pred_idx,
                    "score": score,
                }
            )
        # correct += pred.eq(target.view_as(pred)).sum().item()
        # sum up batch loss
        if batch_idx == len(batch) - 1:
            import pandas as pd

            df = pd.DataFrame(self.rows)
            from kwcoco.metrics import confusion_vectors

            cfsn_vecs = confusion_vectors.ConfusionVectors.from_arrays(
                true=df["true"].values,
                pred=df["pred"].values,
                score=df["score"].values,
                classes=self.classes,
            )
            report = cfsn_vecs.classification_report()
            confusion = report["confusion"]
            metrics = report["metrics"]
            print("Test Confusion")
            print("-------------")
            rich.print(confusion)
            print("Test Metrics")
            print("------------")
            rich.print(metrics)
            combined_metrics = metrics.loc["combined"]
            # self.log("val_loss", loss,prog_bar=True)
            # return loss

    def configure_optimizers(self):
        if self.config.optimizer == "Adam":
            optimizer = optim.Adam(
                self.parameters(),
                lr=self.config.lr,
                betas=(self.config.b0, self.config.b1),
                weight_decay=self.config.weight_decay,
            )
        elif self.config.optimizer == "SGD":
            optimizer = optim.SGD(
                self.parameters(),
                lr=self.config.lr,
                momentum=self.config.momentum,
                weight_decay=self.config.weight_decay,
            )
        elif self.config.optimizer == "AdamW":
            optimizer = torch.optim.AdamW(
                self.parameters(),
                lr=self.config.lr,
                betas=(self.config.b0, self.config.b1),
                weight_decay=self.config.weight_decay,
            )
        elif self.config.optimizer == "lamb":
            optimizer = torch_optimizer.lamb.Lamb(
                self.parameters(),
                lr=self.config.lr,
                betas=(self.config.b0, self.config.b1),
                eps=self.config.eps,
                weight_decay=self.config.weight_decay,
            )

        scheduler = optim.lr_scheduler.MultiStepLR(
            optimizer, milestones=[150, 225, 270], gamma=0.1
        )

        return [optimizer], [scheduler]
