"""
Utilities for computing loss-of-plasticity metrics. Much of the code is drawn from
https://github.com/shibhansh/loss-of-plasticity/blob/main/lop/incremental_cifar/post_run_analysis.py 

Note that alternative implementations can also be found in
https://github.com/shibhansh/loss-of-plasticity/blob/main/lop/utils/miscellaneous.py and
https://github.com/shibhansh/loss-of-plasticity/blob/main/lop/permuted_mnist/online_expr.py

However, these are more specific to the incremental-MNIST setting. (Example: their
neural network explicitly outputs all hidden features, whereas this implementation
uses torchvision feature_extractor to obtain hidden features).
"""

import numpy as np
from torch.utils.data import DataLoader
import torch
from line_profiler import profile

@torch.no_grad()
@profile
def compute_average_weight_magnitude(net: torch.nn.Module):
    """Computes average magnitude of the weights in the network.

    Source: https://github.com/shibhansh/loss-of-plasticity/blob/main/lop/incremental_cifar/post_run_analysis.py
    """
    num_weights = 0
    sum_weight_magnitude = torch.tensor(0.0, device=net.fc.weight.device)

    for p in net.parameters():
        num_weights += p.numel()
        sum_weight_magnitude += torch.sum(torch.abs(p))

    return sum_weight_magnitude.cpu().item() / num_weights


def compute_gradient_norm(net: torch.nn.Module, norm_p: int=1):
    """
    Compute average gradient norm. There is a bit of duplicate
    computation with compute_average_weight_magnitude so these could
    potentialy be combined.

    If norm_p is 1, computes the L1 norm (average absolute magnitude).
    If norm_p is 2, computes the L2 norm.

    Source: https://discuss.pytorch.org/t/check-the-norm-of-gradients/27961/5
    """
    num_weights = 0
    sum_gradient_norm = torch.tensor(0.0, device=net.fc.weight.device)

    # Loop through all parameters
    for p in net.parameters():
        num_weights += p.numel()  # Number of elements
        param_norm = p.grad.detach().data.norm(norm_p)  # Norm of this weight vector
        total_norm += param_norm.item() ** norm_p
    total_norm = total_norm ** (1/norm_p)
    return total_norm.cpu().item() / num_weights


@torch.no_grad()
@profile
def compute_dormant_units_proportion(feature_extractor, cifar_data_loader: DataLoader, dormant_unit_threshold: float = 0.01):
    """
    Computes the proportion of dormant units in a ResNet, for the first batch in cifar_data_loader.
    Also returns the last-layer features.

    Note that feature_extractor should be constructed according to the tutorial here:
    https://pytorch.org/vision/stable/feature_extraction.html
    Specifically, create a node for every ReLU activation (see example in gen_matrix.py),
    and make the last node "avgpool" (final feature map). As of Python 3.7 dictionaries
    now preserve order.

    Sources:
    https://github.com/shibhansh/loss-of-plasticity/blob/main/lop/incremental_cifar/post_run_analysis.py
    https://pytorch.org/vision/stable/feature_extraction.html
    """

    device = next(feature_extractor.parameters()).device  # fc.weight.device
    features_per_layer = []
    #last_layer_activations = None
    #num_samples = 1000

    for batch_idx, (image, target) in enumerate(cifar_data_loader):
        image, target = image.to(device), target.to(device)
        post_activations = feature_extractor(image)

        # Post activations is a dict mapping from node_name to feature tensor
        # print("Post activations", [f"{k}: {v.shape}" for k, v in post_activations.items()])
        features_per_layer = list(post_activations.values())[:-1]  # All but last feature map
        last_layer_features = list(post_activations.values())[-1]  # Last feature map
        last_layer_features = last_layer_features.reshape((last_layer_features.shape[0], -1))
        break

    dead_neurons = torch.zeros(len(features_per_layer), dtype=torch.float32)
    for layer_idx in range(len(features_per_layer) - 1):
        dead_neurons[layer_idx] = ((features_per_layer[layer_idx] != 0).float().mean(dim=(0, 2, 3)) < dormant_unit_threshold).sum()
    dead_neurons[-1] = ((features_per_layer[-1] != 0).float().mean(dim=0) < dormant_unit_threshold).sum()
    number_of_features = torch.sum(torch.tensor([layer_feats.shape[1] for layer_feats in features_per_layer])).item()
    return dead_neurons.sum().item() / number_of_features, last_layer_features.cpu().numpy()

@profile
def compute_effective_rank(singular_values: np.ndarray):
    """Computes the effective rank of a representation layer, given its singular values
    (which should be precomputed using scipy.linalg.svd; see gen_matrix.py for example)

    Note that singular_values should be a Numpy array. It is also possible to compute
    SVD in PyTorch but this "may fail to converge in gpu". See compute_matrix_rank_summaries() in
    https://github.com/shibhansh/loss-of-plasticity/blob/main/lop/utils/miscellaneous.py

    Source:
    https://github.com/shibhansh/loss-of-plasticity/blob/main/lop/incremental_cifar/post_run_analysis.py
    """

    norm_sv = singular_values / np.sum(np.abs(singular_values))
    entropy = 0.0
    for p in norm_sv:
        if p > 0.0:
            entropy -= p * np.log(p)

    return np.e ** entropy