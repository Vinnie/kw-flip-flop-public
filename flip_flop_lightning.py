import random
import copy
import numpy as np
from lop.algos.res_gnt import ResGnT

# from lop.algos.res_gnt import ResGnT
import torchvision
import torch
import torch_liberator
import torch.nn as nn
import torchvision.transforms as transforms
from torch.utils.data import Subset, DataLoader, random_split
from torchvision.models.feature_extraction import (
    get_graph_node_names,
    create_feature_extractor,
)
import time
import scriptconfig as scfg
import kwutil
import ubelt as ub
import pickle
import rich
import rich.markup
import torch.optim as optim
import torch_optimizer
import lightning as L
import torchmetrics
from lightning.pytorch.callbacks import Callback, ModelCheckpoint
import resnet
import torchvision_modified_resnet
from flip_flop_lightning_modules import LitResnet, LitResnetFullHalf,JustDoTestEveryEpoch


def create_model(config, num_classes):
    if config == "fifty":
        return torchvision.models.resnet50(num_classes=num_classes)
    elif config == "eightteen":
        return torchvision.models.resnet18(num_classes=num_classes)
    elif config == "thirtyfour":
        return torchvision.models.resnet34(num_classes=num_classes)
    elif config == "eightteen_small":
        return torchvision_modified_resnet.build_resnet18(
            num_classes=num_classes, norm_layer=torch.nn.BatchNorm2d
        )
    elif config == "thirtyfour_small":
        return torchvision_modified_resnet.build_resnet34(
            num_classes=num_classes, norm_layer=torch.nn.BatchNorm2d
        )


class FlipFLopCLI(scfg.DataConfig):
    method = scfg.Value(
        "eightteen",
        choices=[
            "eightteen",
            "fifty",
            "thirtyfour",
            "eightteen_small",
            "thirtyfour_small",
            "half",
            "full",
        ],
        help="Choose which method to implement, eightteen and fifty are default resnets",
    )
    method_note = scfg.Value(
        "",
        help="Additional note on the method, to use in the filepath. For example, distinguishing between different versions of 'half'.",
    )
    flipflop_freq = scfg.Value(
        1,
        help="For full/half methods, number of epochs to train a model before flipflopping",
    )
    resnetA = scfg.Value(
        "eightteen",
        choices=[
            "eightteen",
            "fifty",
            "thirtyfour",
            "eightteen_small",
            "thirtyfour_small",
        ],
        help="if half or full is selected allowe user to pick two networks to use torch_liberator on",
    )
    resnetB = scfg.Value(
        "fifty",
        choices=[
            "eightteen",
            "fifty",
            "thirtyfour",
            "eightteen_small",
            "thirtyfour_small",
        ],
        help="if half or full is selected allowe user to pick two networks to use torch_liberator on",
    )

    # Knowledge distillation parameters (only used in "half" method going from resnetB to resnetA)
    distill_epochs = scfg.Value(
        0, help="Num epochs to distill (only when going from resnetB to resnetA)"
    )
    T = scfg.Value(
        2,
        help="Distillation temp (see T in https://pytorch.org/tutorials/beginner/knowledge_distillation_tutorial.html)",
    )
    lambda_distillation = scfg.Value(0.25, help="Weight on distillation loss.")

    # Optimization parameters
    optimizer = scfg.Value(
        "SGD",
        choices=["SGD", "Adam", "AdamW", "lamb"],
        help="specify optimizer to be used",
    )
    lr = scfg.Value(1e-2, help="set Learning Rate")
    weight_decay = scfg.Value(5e-4, help="Weight decay")
    momentum = scfg.Value(0.9, help="Momentum")
    b0 = scfg.Value(0.9, help="specify Beta for Adam/Lamb")
    b1 = scfg.Value(0.999, help="specify 2nd Beta for Adam/Lamb")
    eps = scfg.Value(1e-06, help="specify number for stability ")
    additional_transform = scfg.Value(
        False,
        help="applies additonal transforms to the training dataset. right now random crops and flips",
    )
    # Hardware
    gpu = scfg.Value(0, help="specify a gpu")
    out_path = scfg.Value("./training_flipflop", help="specify an output path")

    batches = scfg.Value(1000, help="size of batches")
    epochs = scfg.Value(1000, help="number of epochs")
    save_model = scfg.Value(True, help="Save Model After every epoch")
    workers = scfg.Value(2, help="number of dataloader workers")

    seed = scfg.Value(345, help="seed")

    # Dataset/experiment settings
    dataset = scfg.Value(
        "cifar10", choices=["cifar10", "cifar100"], help="Dataset to train/test on"
    )
    experiment_type = scfg.Value(
        "data_incremental", choices=["class_continual", "data_incremental"]
    )
    num_tasks = scfg.Value(
        2,
        help="Number of tasks. If experiment_type is class_continual, each task will add 'num_classes/num_tasks' classes. If experiment_type is data_incremental, each task will add 'num_examples/num_tasks' examples.",
    )

    # network resetting parameters
    reset_head = scfg.Value(False, help="Whether to reset head after each task")
    reset_network = scfg.Value(
        False, help="Wherther to reset entire network after each task"
    )

    # Continual backprop parameters
    use_cbp = scfg.Value(False, help="Whether to use continual backprop")
    replacement_rate = scfg.Value(
        0.0, help="How many units should be replaced each epoch"
    )
    utility_function = scfg.Value("weight", choices=["weight", "contribution"])
    maturity_threshold = scfg.Value(
        0,
        help="How many epochs to protect reinitialized units until they can be reset again",
    )

    # Shrink and perturb parameters
    noise_std = scfg.Value(0.0, help="Noise for shrink-and-perturb")

    def __post_init__(self):
        """
        Checks to make sure config is internally-consistent, and adds
        any derived variables
        """
        assert (not self.use_cbp) or (
            self.replacement_rate > 0.0
        ), "Replacement rate should be greater than 0."
        assert (not self.use_cbp) or (
            self.maturity_threshold > 0
        ), "Maturity threshold should be greater than 0."
        self.perturb_weights_indicator = self.noise_std > 0.0


def main():
    """
    IGNORE: from flip_flop import *
    """
    config = FlipFLopCLI.cli(cmdline=1, special_options=False)
    rich.print(rich.markup.escape(ub.urepr(config)))

    method = config.method

    time0 = time.time()
    # Directly load resnet50 state into a model that has it as an embedded subnetwork
    # load partial state returns information about what it did
    # Training settings
    batch_size = config.batches
    epochs = config.epochs
    no_cuda = False
    use_cuda = not no_cuda and torch.cuda.is_available()

    # For reproducibility (TODO change to geowatch seed_everything?)
    L.seed_everything(config.seed)
    # print(f'device={device}')

    if config.additional_transform == True:
        # Training Data augmentation
        transform_train = transforms.Compose(
            [
                transforms.RandomCrop(32, padding=4),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize(
                    (0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)
                ),
            ]
        )

        # Testing Data preparation

    else:
        # Training Data augmentation
        transform_train = transforms.Compose(
            [
                transforms.ToTensor(),
                transforms.Normalize(
                    (0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)
                ),
            ]
        )
        # Testing Data preparation
        transform_test = transforms.Compose(
            [
                transforms.ToTensor(),
                transforms.Normalize(
                    (0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)
                ),
            ]
        )
    transform_test = transforms.Compose(
        [
            transforms.ToTensor(),
            transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
        ]
    )

    # Set up datasets
    if config.dataset == "cifar10":
        train_val_set = torchvision.datasets.CIFAR10(
            root="./data", train=True, download=True, transform=transform_train
        )
        testset = torchvision.datasets.CIFAR10(
            root="./data", train=False, download=True, transform=transform_test
        )

        # Overwrite class labels to use shorter terms for "airplane" and "automobile"
        CIFAR10_CLASSES = (
            "plane",
            "car",
            "bird",
            "cat",
            "deer",
            "dog",
            "frog",
            "horse",
            "ship",
            "truck",
        )
        classes = CIFAR10_CLASSES
        train_val_set.classes = CIFAR10_CLASSES
        testset.classes = CIFAR10_CLASSES

    elif config.dataset == "cifar100":
        train_val_set = torchvision.datasets.CIFAR100(
            root="./data", train=True, download=True, transform=transform_train
        )
        testset = torchvision.datasets.CIFAR100(
            root="./data", train=False, download=True, transform=transform_test
        )
        classes = train_val_set.classes
    else:
        raise ValueError("dataset not supported (choose cifar10 or cifar100)")

    # Split "training" set into train/validation splits.
    # Use a generator so it is exactly reproducible
    generator = torch.Generator().manual_seed(config.seed)
    trainset, valset = random_split(train_val_set, [0.9, 0.1], generator=generator)

    # "random_split" does not preserve custom fields, such as
    # "targets" (label of each example) and "classes" (list of
    # class names). Manually port those over to trainset and valset.
    # https://github.com/pytorch/pytorch/issues/97944

    device = torch.device(f"cuda:{config.gpu}" if use_cuda else "cpu")
    trainset.targets = torch.tensor(
        [train_val_set.targets[i] for i in trainset.indices], device=device
    )
    valset.targets = torch.tensor(
        [train_val_set.targets[i] for i in valset.indices], device=device
    )
    trainset.classes = train_val_set.classes
    valset.classes = train_val_set.classes

    # Do not augment the validation images.
    valset.transform = transform_test

    # Continual learning simulations: order in which to add classes or examples
    class_order = torch.randperm(len(train_val_set.classes), generator=generator)
    train_order = torch.randperm(len(trainset), generator=generator)
    val_order = torch.randperm(len(valset), generator=generator)
    epochs_per_task = config.epochs // config.num_tasks

    # Subset the train/validation datasets
    if config.experiment_type == "class_continual":
        # Add additional classes
        new_classes_per_task = len(trainset.classes) // config.num_tasks
        current_num_classes = new_classes_per_task
        curr_classes = class_order[:current_num_classes]

        # Select indices of examples that match those classes
        train_indices = torch.isin(trainset.targets, curr_classes).nonzero().flatten()
        trainset_task = Subset(trainset, train_indices)
        val_indices = torch.isin(valset.targets, curr_classes).nonzero().flatten()
        valset_task = Subset(valset, val_indices)
    elif config.experiment_type == "data_incremental":
        # Add additional datapoints
        current_fraction = 1 / config.num_tasks
        n_train = int(current_fraction * len(train_order))
        n_val = int(current_fraction * len(val_order))
        trainset_task = Subset(trainset, train_order[:n_train])
        valset_task = Subset(valset, val_order[:n_val])
    else:
        raise ValueError("Invalid experiment_type")

    # Also add targets/classes to the subset. We can use simpler
    # indexing syntax as trainset.targets/valset.targets are already Tensors.
    trainset_task.targets = trainset.targets[trainset_task.indices]
    valset_task.targets = valset.targets[valset_task.indices]
    trainset_task.classes = train_val_set.classes
    valset_task.classes = train_val_set.classes

    # Create dataloaders
    train_loader = DataLoader(
        trainset_task, batch_size=batch_size, shuffle=True, num_workers=config.workers
    )
    val_loader = DataLoader(
        valset_task, batch_size=batch_size, shuffle=False, num_workers=config.workers
    )
    test_loader = DataLoader(
        testset, batch_size=batch_size, shuffle=False, num_workers=config.workers
    )
    train_loader.classes = classes
    val_loader.classes = classes
    test_loader.classes = classes
    num_classes = len(classes)

    # Set up models
    # TODO not sure if default initialization is good enough
    """
    model18 = torchvision.models.resnet18(num_classes=num_classes)
    model50 = torchvision.models.resnet50(num_classes=num_classes)
    model18.to(device)
    model50.to(device)
    """
    if config.method in [
        "eightteen",
        "thirtyfour",
        "fifty",
        "eightteen_small",
        "thirtyfour_small",
        "fifty_small",
    ]:
        if config.use_cbp:
            # Set up generate-and-test algorithm (continual backprop).
            # Computes utilty of each neuron and resets the least-useful ones
            model = create_model(config.method, num_classes=num_classes)

            # First, use torchvision's feature extractor to convert both models
            # into models that reutrn post-activation features.
            train_nodes, eval_nodes = get_graph_node_names(model)
            print("Train nodes", train_nodes)

            # Replace last post-activaiton with flattened. "fc" returns the output logits
            post_activations = [n for n in train_nodes if "relu" in n][:-1] + [
                "flatten",
                "fc",
            ]
            print("Post activations", post_activations)
            return_nodes = {k: k for k in post_activations}
            model = create_feature_extractor(model, return_nodes=return_nodes)
            model_init = copy.deepcopy(model)

            # Set up ResGnT objects on top of the networks, which can track
            # the utility of each unit and has functions to replace low-utility units.
            resgnt = ResGnT(
                net=model,
                hidden_activation="relu",
                replacement_rate=config.replacement_rate,
                decay_rate=0.99,
                util_type=config.utility_function,
                maturity_threshold=config.maturity_threshold,
                device=device,
            )
        else:
            # Create the single-model (no generate-and-test)
            model = create_model(config.method, num_classes=num_classes)
            resgnt = None
        model = LitResnet(model, classes=classes, config=config, resgnt=resgnt)

    elif config.method == "half" or config.method == "full":
        if config.use_cbp:
            # Set up generate-and-test algorithm (continual backprop) for modelA.
            # See comment above
            modelA = create_model(config.resnetA, num_classes=num_classes)
            train_nodes_A, eval_nodes_A = get_graph_node_names(modelA)
            post_activations_A = [n for n in train_nodes_A if "relu" in n][:-1] + [
                "flatten",
                "fc",
            ]
            return_nodes_A = {k: k for k in post_activations_A}
            modelA = create_feature_extractor(modelA, return_nodes=return_nodes_A)
            modelA.to(device)

            # Repeat for modelB
            modelB = create_model(config.resnetB, num_classes=num_classes)
            train_nodes_B, eval_nodes_B = get_graph_node_names(modelB)
            post_activations_B = [n for n in train_nodes_B if "relu" in n][:-1] + [
                "flatten",
                "fc",
            ]
            return_nodes_B = {k: k for k in post_activations_B}
            modelB = create_feature_extractor(modelB, return_nodes=return_nodes_B)
            modelB.to(device)

            # Set up ResGnT objects on top of the networks, which can track
            # the utility of each unit and has functions to replace low-utility units.
            resgntA = ResGnT(
                net=modelA,
                hidden_activation="relu",
                replacement_rate=config.replacement_rate,
                decay_rate=0.99,
                util_type=config.utility_function,
                maturity_threshold=config.maturity_threshold,
                device=device,
            )
            resgntB = ResGnT(
                net=modelB,
                hidden_activation="relu",
                replacement_rate=config.replacement_rate,
                decay_rate=0.99,
                util_type=config.utility_function,
                maturity_threshold=config.maturity_threshold,
                device=device,
            )
        else:
            # Create both models (no generate-and-test)
            modelA = create_model(config.resnetA, num_classes=num_classes)
            modelA.to(device)
            modelB = create_model(config.resnetB, num_classes=num_classes)
            modelB.to(device)
            resgntA, resgntB = None, None

        info = torch_liberator.load_partial_state(
            modelA, modelB.state_dict(), association="isomorphism", verbose=0
        )
        # model = LitResnetFullHalf(create_model(config.method,num_classes=num_classes),classes=num_classes,config=config,resgnt=resgnt)
        model = LitResnetFullHalf(
            resnetA=modelA,
            resnetB=modelB,
            classes=classes,
            config=config,
            resgntA=resgntA,
            resgntB=resgntB,
        )

    checkpoint = ModelCheckpoint(
        dirpath=config.out_path,
        save_on_train_epoch_end=config.save_model,
        enable_version_counter=True,
        save_top_k=-1,
    )
    total_epoch = 0

    while total_epoch < config.epochs:
        trainer = L.Trainer(
            max_epochs=epochs_per_task,
            log_every_n_steps=1,
            check_val_every_n_epoch=1,
            accelerator="gpu",
            devices=[config.gpu],
            callbacks=[checkpoint,JustDoTestEveryEpoch(testset=test_loader)],
        )
        # trainer.fit(model=model18, train_dataloaders=train_loader)
        trainer.fit(
            model=model, train_dataloaders=train_loader, val_dataloaders=val_loader
        )
        if config.experiment_type == "class_continual":
            # Add additional classes
            current_num_classes += new_classes_per_task
            curr_classes = class_order[:current_num_classes]

            # Select indices of examples that match those classes
            train_indices = (
                torch.isin(trainset.targets, curr_classes).nonzero().flatten()
            )
            trainset_task = Subset(trainset, train_indices)
            val_indices = torch.isin(valset.targets, curr_classes).nonzero().flatten()
            valset_task = Subset(valset, val_indices)
        elif config.experiment_type == "data_incremental":
            # Add additional datapoints
            current_fraction += 1 / config.num_tasks
            n_train = int(current_fraction * len(train_order))
            n_val = int(current_fraction * len(val_order))
            trainset_task = Subset(trainset, train_order[:n_train])
            valset_task = Subset(valset, val_order[:n_val])
        else:
            raise ValueError("Invalid experiment_type")
        trainset_task.targets = trainset.targets[trainset_task.indices]
        valset_task.targets = valset.targets[valset_task.indices]
        trainset_task.classes = train_val_set.classes
        valset_task.classes = train_val_set.classes

        # Reset dataloaders to load the new data subset
        train_loader = DataLoader(
            trainset_task,
            batch_size=batch_size,
            shuffle=True,
            num_workers=config.workers,
        )
        val_loader = DataLoader(
            valset_task,
            batch_size=batch_size,
            shuffle=False,
            num_workers=config.workers,
        )

    # model18.to(device)
    # model50.to(device)
    # Initialize model18 from model50


if __name__ == "__main__":
    main()
