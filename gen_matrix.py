"""
After run_all_flip_flop.py has been run, produce plots of accuracy/F1
and loss-of-plasticity metrics (average weight magnitude, proportion
of dormant units, effective rank) over time, for each method.

For each method, there should be a model checkpoint saved on disk after each epoch.
"""

import argparse
import os
import kwplot
import pandas as pd
import torchvision
import torch
import torch_liberator
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import random_split
import torch.optim as optim
import torchvision
from torchvision.models.feature_extraction import get_graph_node_names, create_feature_extractor
import torchvision.transforms as transforms
import time
import scriptconfig as scfg
import ubelt as ub
from sklearn.metrics import confusion_matrix
import numpy as np
import glob
import kwcoco
import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import svd
from line_profiler import profile
from flip_flop import create_model

from util import compute_average_weight_magnitude, compute_dormant_units_proportion, compute_effective_rank, compute_gradient_norm

# Training Data augmentation
transform_train = transforms.Compose(
    [
        transforms.ToTensor(),
        transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
    ]
)
transform_test = transforms.Compose(
    [
        transforms.ToTensor(),
        transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
    ]
)

@profile
def test(model, device, test_loader):
    model.eval()
    test_loss = 0
    correct = 0
    predicted = np.zeros(10)
    true = np.zeros(10)

    rows = []
    classes = (
        "plane",
        "car",
        "bird",
        "cat",
        "deer",
        "dog",
        "frog",
        "horse",
        "ship",
        "truck",
    )


    with torch.no_grad():
        for data, target in ub.ProgIter(test_loader):
            data, target = data.to(device), target.to(device)
            output = model(data)
            # test_loss += F.nll_loss(
            #     output, target, reduction="sum"
            # ).item()  # sum up batch loss
            pred = output.argmax(
                dim=1, keepdim=True
            )  # get the index of the max log-probability
            batch_probs = output.cpu().numpy()
            pred_idxs = pred.cpu().numpy().ravel()
            true_idxs = target.cpu().numpy().ravel()

            for pred_idx, true_idx, probs in zip(pred_idxs, true_idxs, batch_probs):
                score = probs[pred_idx]
                rows.append({
                    'true': true_idx,
                    'pred': pred_idx,
                    'score': score,
                })
            correct += pred.eq(target.view_as(pred)).sum().item()

    acc = correct / len(test_loader.dataset)  # Overall accuracy

    import pandas as pd
    df = pd.DataFrame(rows)
    from kwcoco.metrics import confusion_vectors

    cfsn_vecs = confusion_vectors.ConfusionVectors.from_arrays(
        true=df['true'].values,
        pred=df['pred'].values,
        score=df['score'].values,
        classes=classes,
    )
    report = cfsn_vecs.classification_report()
    combined_metrics = report['metrics'].loc['combined']
    return combined_metrics['f1'], acc


    # test_loss /= len(test_loader.dataset)
    # print(predicted)
    # print(true)
    # x = confusion_matrix(true,predicted)

    # print(x)

    # print(
    #     "\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n".format(
    #         test_loss,
    #         correct,
    #         len(test_loader.dataset),
    #         100.0 * correct / len(test_loader.dataset),
    #     )
    # )


@profile
def compute_metrics(path, epochs, train_loader, test_loader, model, extractor, device, plot_freq):
    """
    For each model matched by the specified path, compute metrics for that model:
    (1) F1 (test set)
    (2) Accuracy (test set)
    (3) Average weight magnitude
    (4) Proportion dormant units (train set)
    (5) Effective rank of final-layer features (train set)
    (6) Proportion dormant units (test set)
    (7) Effective rank of final-layer features (test set)

    path should be a relative path string that contains one blank (for the epoch). For example,
    `eightteen/resnet18/resnet18_{}.pt`, where the {} will be filled with each epoch number.
    """
    metrics = dict(
        epoch_num = [],
        f1s = [],
        accs = [],

        # Loss-of-plasticity statistics
        weight_magnitudes = [],
        dormant_units_train = [],
        effective_ranks_train = [],
        dormant_units_test = [],
        effective_ranks_test = []
    )

    # Loop through all model files that the path matches
    for epoch in range(1, epochs + 1, plot_freq):
        file_path = path.format(epoch)
        if not os.path.exists(file_path):
            print("Model file not found", file_path)
            continue
        print("Processing model", file_path)

        # TODO Don't assume the device
        load_start = time.time()
        model.load_state_dict(torch.load(file_path, map_location=device))
        model = model.to(device)

        model_start = time.time()
        f1, acc = test(model, device, test_loader)
        metrics["epoch_num"].append(epoch)
        metrics["f1s"].append(f1)
        metrics["accs"].append(acc)

        # Loss-of-plasticity metrics
        # Average weight magnitude
        # TODO try running without weight decay
        metrics["weight_magnitudes"].append(compute_average_weight_magnitude(model))
        metrics["gradient_magnitudes"].append(compute_gradient_norm(model, norm_p=1))

        # Dormant units proportion
        prop_dormant_train, last_layer_features_train = compute_dormant_units_proportion(extractor, train_loader)
        metrics["dormant_units_train"].append(prop_dormant_train)

        # Effective rank
        singular_values_train = svd(last_layer_features_train, compute_uv=False, lapack_driver="gesvd")
        metrics["effective_ranks_train"].append(compute_effective_rank(singular_values_train))

        # Dormant units, effective rank but for test examples
        prop_dormant_test, last_layer_features_test = compute_dormant_units_proportion(extractor, test_loader)
        metrics["dormant_units_test"].append(prop_dormant_test)
        singular_values_test = svd(last_layer_features_test, compute_uv=False, lapack_driver="gesvd")
        metrics["effective_ranks_test"].append(compute_effective_rank(singular_values_test))

    return metrics


def model_str(number_str):
    """Tiny helper that converts a number string ('eightteen') into 'resnet18'"""
    if number_str == "eightteen":
        return "resnet18"
    elif number_str == "thirtyfour":
        return "resnet34"
    elif number_str == "fifty":
        return "resnet50"
    if number_str == "eightteen_small":
        return "resnet18_small"
    elif number_str == "thirtyfour_small":
        return "resnet34_small"
    elif number_str == "fifty_small":
        return "resnet50_small"
    else:
        raise ValueError(f"Invalid number str {number_str}")


def main():
    # TODO Load these from the train config instead
    parser = argparse.ArgumentParser()

    parser.add_argument('--seed', type=int, default=345, help="Random seed used in flip_flop.py's config")
    parser.add_argument('--workers', type=int, default=4, help="DataLoader workers")

    parser.add_argument('--result_dir', type=str, default="./results")
    parser.add_argument('--plot_dir', type=str, default="./plots")
    parser.add_argument('--plot_freq', type=int, default=5, help="Epochs between graph points")

    parser.add_argument('--dataset', type=str, choices=["cifar10", "cifar100"], default="cifar10")
    parser.add_argument('--epochs', type=int, default=10)
    parser.add_argument('--gpu', type=int, default=0)
    parser.add_argument('--resnetA', default='eightteen')
    parser.add_argument('--resnetB', default='thirtyfour')
    # parser.add_argument('--resnetC', default='eightteen_small')

    args = parser.parse_args()
    device = torch.device(f"cuda:{args.gpu}" if torch.cuda.is_available() else "cpu")
    RESULT_DIR = ub.Path(args.result_dir)
    PLOT_DIR = (ub.Path(args.plot_dir) / os.path.basename(args.result_dir)).ensuredir()

    # Set up datasets
    if args.dataset == "cifar10":
        train_val_set = torchvision.datasets.CIFAR10(
            root="./data", train=True, download=True, transform=transform_train
        )
        testset = torchvision.datasets.CIFAR10(
            root="./data", train=False, download=True, transform=transform_test
        )

        # Overwrite class labels to use shorter terms for "airplane" and "automobile"
        CIFAR10_CLASSES = (
            "plane",
            "car",
            "bird",
            "cat",
            "deer",
            "dog",
            "frog",
            "horse",
            "ship",
            "truck",
        )
        train_val_set.classes = CIFAR10_CLASSES
        testset.classes = CIFAR10_CLASSES

    elif args.dataset == "cifar100":
        train_val_set = torchvision.datasets.CIFAR100(
            root="./data", train=True, download=True, transform=transform_train
        )
        testset = torchvision.datasets.CIFAR100(
            root="./data", train=False, download=True, transform=transform_test
        )
    else:
        raise ValueError("dataset not supported (choose cifar10 or cifar100)")

    # Get a subset of the train set used since the beginning.
    # This requires setting the same config.seed as flip_flop.py.
    # Only data_incremental supported for now. TODO this is messy
    generator = torch.Generator().manual_seed(args.seed)
    trainset, valset = torch.utils.data.random_split(train_val_set, [2/3., 1/3.], generator=generator)
    class_order = torch.randperm(len(train_val_set.classes), generator=generator)  # only to maintain random consistency with flip_flop.py
    train_order = torch.randperm(len(trainset), generator=generator)
    val_order = torch.randperm(len(valset), generator=generator)
    trainset_sample = torch.utils.data.Subset(trainset, train_order[0:1000])

    # Create DataLoaders
    train_loader = torch.utils.data.DataLoader(
        trainset_sample, batch_size=1000, shuffle=True, num_workers=args.workers,
    )
    test_loader = torch.utils.data.DataLoader(
        testset, batch_size=1000, shuffle=True, num_workers=args.workers,
    )

    # Initialize model states and feature extractors. The feature extractors
    # allow us to extract intermediate features outputted by the model
    # (we select all post-ReLU features to compute proportion of dead neurons,
    # as well as the last hidden layer).
    # See tutorials:
    # https://pytorch.org/vision/stable/feature_extraction.html
    # https://pytorch.org/vision/main/generated/torchvision.models.feature_extraction.create_feature_extractor.html
    modelA = create_model(args.resnetA, len(testset.classes))
    train_nodes_A, eval_nodes_A = get_graph_node_names(modelA)
    post_activations_A = [n for n in train_nodes_A if "relu" in n] + ["flatten"]
    return_nodes_A = {k: k for k in post_activations_A}
    extractorA = create_feature_extractor(modelA, return_nodes=return_nodes_A)
    modelB = create_model(args.resnetB, len(testset.classes))
    train_nodes_B, eval_nodes_B = get_graph_node_names(modelB)
    post_activations_B = [n for n in train_nodes_B if "relu" in n] + ["flatten"]
    return_nodes_B = {k: k for k in post_activations_B}
    extractorB = create_feature_extractor(modelB, return_nodes=return_nodes_B)
    # modelC = create_model(args.resnetC, len(testset.classes))
    # train_nodes_C, eval_nodes_C = get_graph_node_names(modelC)
    # post_activations_C = [n for n in train_nodes_C if "relu" in n] + ["flatten"]
    # return_nodes_C = {k: k for k in post_activations_C}
    # extractorC = create_feature_extractor(modelB, return_nodes=return_nodes_C)


    # Each entry is a single run (line on the plot).
    # "SUBDIRS" stores the subdirectory of this run inside RESULT_DIR.
    # "METHODS" stores readable method names (maybe this should be the same as SUBDIRS).
    # "MODELS" stores whether the method was run on architecture "A", "B", or "both"
    # TODO maybe make this a commandline argument?
    # TODO add seed
    SUBDIRS = ["test_continual_reset", "test_continual", "test_continual_half",
               "test_continual_half5", "test_continual_half_cbp", "test_continual_half_sp"]
    METHODS = ["Full reset", "Standard training", "Half", "Half 5 with distillation",
               "Half + Continual backprop", "Half + Shrink-and-perturb"]
    MODELS = ["A", "A", "both", "both", "both", "both"]

    # Get metrics for each run
    method_results = {}
    for i in range(len(SUBDIRS)):
        if MODELS[i] in ["A", "both"]:
            method_results[(METHODS[i], model_str(args.resnetA))] = compute_metrics(
                os.path.join(RESULT_DIR, SUBDIRS[i], (rf'{args.resnetA}/{args.resnetA}' + '_{}.pt')),
                args.epochs, train_loader, test_loader, modelA, extractorA, device, args.plot_freq
            )
        if MODELS[i] in ["B", "both"]:
            method_results[(METHODS[i], model_str(args.resnetB))] = compute_metrics(
                os.path.join(RESULT_DIR, SUBDIRS[i], (rf'{args.resnetB}/{args.resnetB}' + '_{}.pt')),
                args.epochs, train_loader, test_loader, modelB, extractorB, device, args.plot_freq
            )
 
    print("Start seaborn")

    for (method, architecture), metrics in method_results.items():
        # add a label for each result.
        metrics['method'] = [method] * len(metrics['epoch_num'])
        metrics['architecture'] = [architecture] * len(metrics['epoch_num'])

    # Bring everything into one giant table
    data = pd.concat([pd.DataFrame(v) for v in method_results.values()])
    data.to_csv(PLOT_DIR / "all_metrics.csv")

    sns = kwplot.autosns()  # Helper to import seaborn

    metric_names = sorted(set(data.keys()) - {'epoch_num', 'method', 'architecture'})
    for metric_name in metric_names:
        fig = kwplot.figure(fnum=metric_name)
        ax = fig.gca()
        sns.lineplot(data=data, x='epoch_num', y=metric_name, hue='method', style='architecture')
        ax.set_title(f"{metric_name} by method")
        fig.savefig(PLOT_DIR / f"{metric_name}.png")



if __name__ == "__main__":
    main()
